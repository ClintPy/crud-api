import express from 'express';
import postController from '../controllers/postController';

const router = express.Router();

router.get('/api/v1/posts/', postController.getAllPost);
router.get('/api/v1/posts/:id', postController.getOnePost);
router.post('/api/v1/posts', postController.createPost);

export default router
