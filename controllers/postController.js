import Posts from "../db/posts"
import uuid from "uuid";
import moment from "moment";

class postsController {
    static getPosts(req,res){
        return res.json({
            message: "List of all posts",
            posts: Posts
        })
    }

    static createPost(req,res){
        const { title, body } = req.body;
        const newPost = {
            id: uuid.v4(),
            title,
            body,
            created_at: moment.utc().format()
        }
        Posts.push(newPost);
        return res.status(200).json({
            message: "created a new Post"
        })
    }

    static getOnePost(req,res){
        const { id } = req.params;
        const post = Posts.find(onePost => onePost.id == id);
        if(post){
            return res.status(200).json({
                message: "one post found",
                onePost: post
            })
        } else {
            res.status(400).json({
                error: "no post found with that id"
            })
        }
    }

    static getAllPost(req,res){
        const posts = Posts
        if(posts){
            return res.status(200).json({
                message: "Posts Found",
                allPosts: posts
            })
        } else {
            res.status(400).json({
                error: "no posts currently available"
            })
        }
    }
}

export default postsController